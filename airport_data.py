from sqlalchemy import Column, String, Float
from sqlalchemy.ext.declarative import declarative_base


class Airport(declarative_base()):
    __tablename__ = "airports_data"
    id = Column('id', String(32), primary_key=True)
    type = Column('type', String(64))
    name = Column('name', String(256))
    lat = Column('lat', Float())
    lon = Column('lon', Float())

    def __init__(self, id, type, name, lat, lon):
        self.id = id
        self.type = type
        self.name = name
        self.lat = lat
        self.lon = lon
