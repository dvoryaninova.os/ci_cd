#!/bin/bash -l
if [ -z "$1" ]; then
    echo 'arg <project name>'
    exit 1
fi

apt update && apt install -y ntp mc htop screen wget docker.io docker docker-compose build-essential python-dev libreadline-dev libbz2-dev libssl-dev libsqlite3-dev libxslt1-dev libxml2-dev git curl mysql-client libmysqlclient-dev python3-dev
systemctl enable docker
service ntp restart


docker-compose down
docker-compose kill
docker volume prune


cp -f server.docker-compose.yml docker-compose.yml
#[ -f ./init.sh ] && ./init.sh || exit 1
docker-compose up -d --build --force-recreate --remove-orphans
#[ -f ./post_init.sh ] && ./post_init.sh || exit 1